class Game:
    def __init__(self, id):
        self.player_id = id
        self.visible_word = []
        self.wrong_guesses = []
        self.guesses_left = 5
    
    def generate_visible_word(self, word):
        for letter in word:
            self.visible_word += '_'