from datetime import datetime
import random
import paho.mqtt.client as mqtt
import jsonpickle
import json

class Room:
    def __init__(self, id, creator):
        self.id = id
        self.creator = creator
        self.participants = []
        self.losers = []
        self.date = datetime.now().strftime('%m/%d/%Y, %H:%M:%S')
        self.game_status = 'waiting'
        self.word = ''
        self.players_turn = ''

    def generate_word(self, category):
        self.category = category
        lines = open('forca_backend/static/' + category + '.txt').read().splitlines()
        selected_word = random.choice(lines)

        print('the selected word for the room ' + self.id + ' is ' + selected_word + ' of the category ' + category)
        
        self.word = selected_word

        return self.word
    
    def publish_update(self, event, mqtt_client):
        self.event = event
        mqtt_client.publish('room/' + self.id, jsonpickle.encode(self), qos=0, retain=False)
    
    def add_player(self, player_id, mqtt_client):
        self.participants.append(player_id)
        
        mqtt_client.subscribe("user_channel/"+player_id)

        user_response = {"event":"JOINED", "room":self.id}
        mqtt_client.publish('user_channel/' + player_id, json.dumps(user_response), qos=0, retain=False)

        self.publish_update("GAME_LOBBY", mqtt_client)
        
        print(player_id + " has joined the room " + self.id)

    def remove_player(self, player_id, mqtt_client):
        if player_id in self.participants:
            self.participants.remove(player_id)
        elif player_id in self.losers:
            self.losers.remove(player_id)
        else:
            return False

        user_response = {"event":"LEFT", "room":self.id}
        mqtt_client.publish('user_channel/' + player_id, json.dumps(user_response), qos=0, retain=False)
        
        mqtt_client.unsubscribe('user_channel/' + player_id)

        self.publish_update("GAME_UPDATE", mqtt_client)
        
        print(player_id + " has left the room " + self.id)

        return True
    
    def finish_game(self, winner, mqtt_client):
        pass
    
    def __getstate__(self):
        state = self.__dict__.copy()
        # del state['word']
        return state

    def __setstate__(self, state):
        self.__dict__.update(state)