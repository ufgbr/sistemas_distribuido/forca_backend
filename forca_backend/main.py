import paho.mqtt.client as mqtt
import random
import json
import jsonpickle
import unidecode
from Room import Room
from Game import Game

game_room_counter = 1000

rooms = {}

players_games = {}

server_name = "forca.duckdns.org"

events_user_chanel = ('GAME_UPDATE', 'JOINED', 'GAME_WON', 'GAME_LOSE', 'GAME_TIE', 'LEFT')

def on_connect(mqtt_client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    # global control topics
    mqtt_client.subscribe("create")
    mqtt_client.subscribe("find")

def user_exist(rooms, player_id):
    for room in rooms.values():
        if player_id in room.participants:
            return True
    return False

def update_player(mqtt_client, player_game, event):
    player_game.event = event
    mqtt_client.publish('user_chanel/' + player_game.player_id, jsonpickle.encode(player_game), qos=0, retain=False)

def finish_game(mqtt_client, game_room, winner):
    game_room.status = 'finished'
    
    if winner != None:
        game_room.winner = winner
        
        print(winner + " has won the game on room " + game_room.id)
        
        update_player(mqtt_client, players_games[winner], "GAME_WON")
        
        game_room.remove_player(winner, mqtt_client)

        participants = list.copy(game_room.participants)
        for player in participants:
            update_player(mqtt_client, players_games[player], "GAME_LOST")
            game_room.remove_player(player, mqtt_client)
        
        losers = list.copy(game_room.losers)
        for player in losers:
            update_player(mqtt_client, players_games[player], "GAME_LOST")
            game_room.remove_player(player, mqtt_client)
    else:
        print('No one won the game on the room ' + game_room.id)
        
        game_room.winner = 'None'
        
        losers = list.copy(game_room.losers)
        for player in losers:
            update_player(mqtt_client, players_games[player], "GAME_TIE")
            game_room.remove_player(player, mqtt_client)

    game_room.publish_update("GAME_FINISHED",mqtt_client)
    
    del rooms[game_room.id]
    
    return True


def on_message(mqtt_client, userdata, msg):
    try:
        payload = json.loads(msg.payload)
    except:
        print('error deconding json')
        return

    topic = msg.topic.split('/')
    # print(topic)
    
    # 'create'
    if topic[0] == "create":
        player_id = payload['client_id']

        if user_exist(rooms, player_id):
            return
        
        global game_room_counter
        game_room_counter += 1
        room_id = 'r'+str(game_room_counter)

        rooms[room_id] = Room(room_id, player_id)

        # Subscribe server to all control topics of the room
        mqtt_client.subscribe('room/'+room_id+"/join")
        mqtt_client.subscribe('room/'+room_id+"/leave")
        mqtt_client.subscribe('room/'+room_id+"/start")
        mqtt_client.subscribe('room/'+room_id+"/finish")

        # Insert player into room
        rooms[room_id].add_player(player_id, mqtt_client)

        print(player_id + " has created room " + room_id)
    # 'find'
    elif topic[0] == "find":
        print('Someone is looking for a game!')
        room_list = []

        for room_id, room in rooms.items():
            if room.game_status == 'waiting':
                room_data = {}
                room_data["room"] = room_id
                room_data["number_of_participants"] = len(room.participants)
                room_list.append(room_data)
        
        mqtt_client.publish('available_rooms', json.dumps(room_list), qos=0, retain=False)
    
     # 'room'
    # 'room/?'
    elif topic[0] == "room":
        # 'room/{id}/join
        if topic[2] == 'join':
            player_id = payload['client_id']
            room_id = topic[1]
            
            if user_exist(rooms, player_id):
                return
            
            rooms[room_id].add_player(player_id, mqtt_client)
        
        # 'room/{id}/leave
        if topic[2] == 'leave':
            player_id = payload['client_id']
            room_id = topic[1]
            
            if not user_exist(rooms, player_id):
                return 

            rooms[room_id].remove_player(player_id, mqtt_client)

            if not rooms[room_id].participants:
                del rooms[room_id]
        
        # 'room/{id}/start
        if topic[2] == 'start':
            user = payload['client_id']
            category = payload['category']

            room_id = topic[1]

            # Only the room creator can start the game
            if rooms[room_id].creator != user:
                return
            
            # Select a word for the game
            word = rooms[room_id].generate_word(category)

            # Define a random turn order
            random.shuffle(rooms[room_id].participants)
    
            # Initialize the turn order counter
            rooms[room_id].current_turn_index = 0
            rooms[room_id].players_turn = rooms[room_id].participants[rooms[room_id].current_turn_index]

            # Create a individual game for each player
            for player_id in rooms[room_id].participants:
                players_games[player_id] = Game(player_id)
                players_games[player_id].generate_visible_word(word)
                
                update_player(mqtt_client, players_games[player_id], "GAME_UPDATE")

            rooms[room_id].game_status = 'playing'
            rooms[room_id].publish_update("GAME_STARTED", mqtt_client)
    
    # 'user_channel'
    elif topic[0] == "user_channel":
        if payload['event'] in events_user_chanel or payload['py/object'] == '__main__.Game':
            return

        player_id = topic[1]
        
        # Find game room and verify if is player turn
        room_found = 0
        for id, room in rooms.items():
            if room.players_turn == topic[1]:
                room_found = id
                break
        else:
            return
        
        guess = payload['guess']

        if guess in players_games[player_id].wrong_guesses:
            update_player(mqtt_client, players_games[player_id], "GAME_ERROR")
            return
        
        if guess in players_games[player_id].visible_word:
            update_player(mqtt_client, players_games[player_id], "GAME_ERROR")
            return

        found = False
        for index, letter in enumerate(rooms[room_found].word):
            if guess == unidecode.unidecode(letter):
                found = True
                players_games[player_id].visible_word[index] = letter

        # Win - Player_id won the game
        if '_' not in players_games[player_id].visible_word:
            finish_game(mqtt_client, rooms[room_found], player_id)
            return
        
        if not found:
            # Next turn
            rooms[room_found].current_turn_index += 1
            
            if rooms[room_found].current_turn_index >= len(rooms[room_found].participants):
                rooms[room_found].current_turn_index = 0

            rooms[room_found].players_turn = rooms[room_found].participants[rooms[room_found].current_turn_index]

            players_games[player_id].wrong_guesses.append(guess)
            players_games[player_id].guesses_left -= 1
            
            # Lose - Player_id lost the game
            if players_games[player_id].guesses_left == 0:
                rooms[room_found].losers.append(player_id)
                rooms[room_found].participants.remove(player_id)
            
        # No players left to play - tie
        if not rooms[room_found].participants:
            finish_game(mqtt_client, rooms[room_found], None)
            return

        rooms[room_found].publish_update("GAME_UPDATE", mqtt_client)
        update_player(mqtt_client, players_games[player_id], "GAME_UPDATE")

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("forca.duckdns.org", 1883, 60)

client.loop_forever()