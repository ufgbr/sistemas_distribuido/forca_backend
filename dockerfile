 FROM	ubuntu:20.04
 ENV	TZ=America/Sao_Paulo

 RUN apt-get update && apt-get install -y python3 python3-pip

 RUN pip3 install paho-mqtt jsonpickle unidecode

COPY . . 
RUN python3 forca_backend/main.py